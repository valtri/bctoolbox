Name:           bctoolbox
Version:        4.5.15
Release:        3%{?dist}
Summary:        Utilities library used by Belledonne Communications software

License:        GPLv3+
URL:            https://github.com/BelledonneCommunications/bctoolbox
Source0:        https://github.com/BelledonneCommunications/bctoolbox/archive/refs/tags/4.5.15.tar.gz#/%{name}-%{version}.tar.gz
# remove standard paths from rpath
Patch0:         %{name}-rpath.patch

BuildRequires:  cmake
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  mbedtls-devel
BuildRequires:  BCUnit-devel

%description
BcToolox is utilities library used by Belledonne Communications software like
belle-sip, mediastreamer2 and liblinphone.

%package        devel
Summary:        Header files and libraries for CUnit development
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains the header files
and libraries for use with %{name} package.


%prep
%setup -q
%patch0 -p1


%build
%cmake -DENABLE_STATIC=OFF -DENABLE_TESTS=ON
%cmake_build


%install
%cmake_install


%ldconfig_scriptlets


%files
%{_libdir}/libbctoolbox.so.1
%{_libdir}/libbctoolbox-tester.so.1
%license LICENSE.txt
%doc CHANGELOG.md
%doc README.md

%files devel
%{_datadir}/%{name}/
%{_includedir}/%{name}/
%{_libdir}/libbctoolbox.so
%{_libdir}/libbctoolbox-tester.so
%{_libdir}/pkgconfig/bctoolbox.pc
%{_libdir}/pkgconfig/bctoolbox-tester.pc


%changelog
